#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup
from PIL import Image
import time

def _split_word_array(data, start, end, height):
    """Split and trim a two-dimensional list to string from given area.
    Arguments:
    data -- The list.
    start, end, height -- The area to split and trim.

    Returns a string object to recognize.
    """

    top = -1
    bottom = -1
    result = []

    for y in range(height):
        for x in range(start, end + 1):
            if data[y][x] == 0:
                if top == -1:
                    top = y
                    break
        if not top == -1:
            break

    for y in range(height - 1, -1, -1):
        for x in range(start, end + 1):
            if data[y][x] == 0:
                if bottom == -1:
                    bottom = y
                    break
        if not bottom == -1:
            break

    if not top == -1 and not bottom == -1:
        for y in range(top, bottom - 1):
            for x in range(start, end + 1):
                if data[y][x] == 0:
                    result.append('1')
                else:
                    result.append('0')

    result_string = ''.join(result)
    # print(result)
    return result_string


def _recognize_letter(string: str, debug: bool = False):
    """Recognize letter from given string.
    Arguments:
    string -- The string to recognize.
    debug -- Print recognition detail.

    Returns a string object with single letter.
    """

    if debug:
        print('Input string:', string)
    letters = [
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
        "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
    ]
    ocr_letters = [
        # a
        "00111111000011111111000100001111000000001110000000011100001111111001111111110111100011101110000111011100011110",
        # b
        "111000000000111000000000111000000000111000000000111000000000111011111000111111111100111100001110111000000111111000000111111000000111111000000111111000000111111000001111111100011110",
        # c
        "0001111110001111111101111000011111000000111000000011100000001110000000111000000011110000000111100001",
        # d
        "000000000111000000000111000000000111000000000111000000000111000111110111001111111111011110001111111100000111111000000111111000000111111000000111111000000111111000000111011100011111",
        # e
        "00011111000001111111100111000111011100000111111000001111111111111111111111111111000000001110000000001110000001",
        # f
        "000011111000111111001110000001110000001110000111111110111111110001110000001110000001110000001110000001110000001110000001110000001110000",
        # g
        "000111110111001111111111011110001111111100000111111000000111111000000111111000000111111000000111111000000111011100011111011111111111000111100111000000000111011000001110",
        # h
        "111000000011100000001110000000111000000011100000001110011110111011111011111001111111000111111000011111100001111110000111111000011111100001111110000111",
        # i
        "111111000000111111111111111111111111111111",
        # j
        "000011100001110000000000000000001110000111000011100001110000111000011100001110000111000011100001110000111000011100001111000111",
        # k
        "111000000000111000000000111000000000111000000000111000000000111000001100111000011000111000110000111001100000111011000000111111100000111011110000111001110000111000111000111000111100",
        # l
        "111111111111111111111111111111111111111111111",
        # m
        "11100111100011110111011111101111111111000111100011111100001110000111111000011100001111110000111000011111100001110000111111000011100001111110000111000011111100001110000111",
        # n
        "1110011110111011111011111001111111000111111000011111100001111110000111111000011111100001111110000111",
        # o
        "000111111000001111111100011100001110111100001111111000000111111000000111111000000111111000000111111100001111011100001110",
        # p
        "111011111000111111111100111100001110111000000111111000000111111000000111111000000111111000000111111000001111111100011110111111111100111011111000111000000000111000000000",
        # q
        "000111110111001111111111011110001111111100000111111000000111111000000111111000000111111000000111111000000111011100011111011111111111000111100111000000000111000000000111",
        # r
        "11100111111011111111100011110000111000001110000011100000111000001110000011100000",
        # s
        "00111100011111101110001011100000111110000111110000111110000011110000011110000111",
        # t
        "011100001110001111111111111101110000111000011100001110000111000011100001110000111100",
        # u
        "1110000111111000011111100001111110000111111000011111100001111110000111111000011111100011111110011111",
        # v
        "111000000011011100000110011100000110011100000110001110001100001110001100001111011000000111011000000111011000000011110000",
        # w
        "11100001111000011111000011110000110111000111100011001110001111000110011100110111001100011101101110110000111011011101100001110110111011000011101000110110000011110001111000",
        # x
        "11110000011011100001100011100110000111111000000111110000001111000000001111000000111110000001111110000110011100",
        # y
        "111000000011011100000110011100000110011110001110001110001100001111011100000111011000000111011000000011110000000011110000000011110000000001100000000011100000000011000000",
        # z
        "11111111111111111111110000000111100000011110000001111000000111100000011110000001111000000111100000011110000000"
    ]
    for i in range(len(ocr_letters)):
        if string == ocr_letters[i]:
            if debug:
                print('Recognized letter:', letters[i])
            return letters[i]

    print('No pattern found')

def get_authcode(auth_image: Image = None, debug: bool = False):
    """Load "auth.jpg" in the folder and return the authcode.
    Arguments:
    auth_image -- Use provide image if available.
    debug -- Print recognition detial.

    Returns a string object with the auth code in auth_image.
    """
    if auth_image:
        image = auth_image
    else:
        image = Image.open('auth.jpg')

    raw_data = image.load()
    gray = []
    auth_list = []
    image_start = -1
    image_end = -1

    for y in range(image.size[1]):
        row = []

        for x in range(image.size[0]):
            if raw_data[x, y] == (255, 255, 255):
                row.append(0)
            else:
                row.append(1)
        gray.append(row)

    for x in range(image.size[0]):
        is_background = True

        for y in range(image.size[1]):
            if gray[y][x] == 0:
                is_background = False
                if image_start == -1:
                    image_start = x

        if is_background and not image_start == -1:
            image_end = x - 1

        if not image_start == -1 and not image_end == -1:
            word_string = _split_word_array(
                data=gray,
                start=image_start,
                end=image_end,
                height=image.size[1])
            # print(word_string)
            auth_letter = _recognize_letter(string=word_string, debug=debug)
            # print(auth_letter)
            auth_list.append(auth_letter)
            image_start = -1
            image_end = -1

    auth_string = ''.join(auth_list)
    return auth_string

# Get current time in millisecond
def get_timeline():
    return str(time.time()*1000//1)[0:-2]
